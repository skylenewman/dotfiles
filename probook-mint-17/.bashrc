source ~/.sources/ls.bashrc
source ~/.sources/scripts.bashrc
source ~/.sources/cd.bashrc

export PS1="\[\e[36m\]\u \w > \[\e[0m\] "
#PS1='\[\e[33m\]MyFancyPrompt\[\e[0m\] '
eval "$(rbenv init -)"
alias la='ls -alh'
alias lsdir='ls -d */'
export PATH="$HOME/.rbenv/bin:$PATH"
export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"
export PATH="$HOME/.rbenv/shims:$PATH"
#source ~/.rbenv/completions/rbenv.bash
#source ~/.profile
PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting
alias filetree='find . -type f -printf "%T@ %p\n" | sort -nr | cut -d\  -f2-'
export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"
alias nano='nano -c'
alias sourceme='source ~/.bashrc'
alias go-field-migrations='cd ~/field_scheduler/migrations'
alias go-field-assets='cd ~/field_scheduler/app/assets'
alias go-field-model='cd ~/field_scheduler/app/lib/models'
alias go-field-routes='cd ~/field_scheduler/app/lib/routes'
alias go-field-lib='cd ~/field_scheduler/app/lib'
alias go-field-views='cd ~/field_scheduler/app/views'
ttitle(){
   echo -en "\033]0;$1\a"
}
alias greppr="ps -ef | grep"
alias sublime_text="/bin/Sublime\ Text\ 2/./sublime_text"
alias lspackages="sudo  dpkg-query -Wf \'${Installed-Size}\t${Package}\n\' | sort -n"

export WP_THEMES="/var/www/wordpress/wp-content/themes"
export WP_PLUGINS="/var/www/wordpress/wp-content/plugins"
export MERLIN="/var/www/merlin/merlin"
alias connectmerlin='ssh -p 4123 excal@excaliburschool.org'
### Added by the Heroku Toolbelt
export PATH="/usr/local/heroku/bin:$PATH"
alias at_home='~/.screenlayout/./home.sh'
PS1="\n\e[1;34m[\u   @   \h   \D{%F   %T}]\n\w\e[m\n> "
alias rm='rm -i'
alias cpi='cp -i'
alias cp='cp'
alias mv='mv -i'
# -> Prevents accidentally clobbering files.
alias mkdir='mkdir -p'
alias l='ls -l'
alias ..='cd ..'
alias bk='cd -'
alias rmdir='rm -rf'
alias greppr='ps -ef | grep'
EXINIT="set nu"; export EXINIT
alias cls='clear;ls'
alias grep='grep -i --color'

alias rmf='rm -rf'

export AWS_ACCESS_KEY=AKIAJ2YRX6YTE4J43C5A
export AWS_SECRET_KEY=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
alias kepler="~/eclipse/kepler/eclipse/./eclipse"
